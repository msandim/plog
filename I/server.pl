:-use_module(library(sockets)).

:- include(input).

port(60070).

% ************************************************************************
% *********************** INITIATE GAME SERVER ***************************
% ************************************************************************

server :-
        port(Port),
        socket_server_open(Port, Socket),
        socket_server_accept(Socket, _Client, Stream, [type(text)]),
        write('** Accepted connection **'), nl,
        serverLoop(Stream),
        socket_server_close(Socket).


% wait for commands
serverLoop(Stream) :-
        repeat,
                read(Stream, GuiMsg), % Read command from the GUI client
                %write('-> Received: '), write(GuiMsg), nl,
                
                parse_msg(GuiMsg, Reply),
                
                format(Stream, '~q~n', [Reply]),
                %write('   Replied: '), write(Reply), nl,
                
                flush_output(Stream),
        (GuiMsg == quit; GuiMsg == end_of_file), !.

% *********************************** Init game ***********************************
parse_msg(init, Reply) :-
        % Initialize variables:
        initialBoard(Board),
        Reply = yes(Board), !.
%                                    ***********
% ********************************** MAKE A PLAY ************************************
%                                    ***********

% *********************************** Human player ***********************************
parse_msg(move(Board, [Color,'H', Score], InitialPosition, FinalPosition), Reply) :-
        checkValidMove(Board, [Color,'H', Score], InitialPosition, FinalPosition, Move), % Check if the move was valid
        makeMove(Board, [Color,'H', Score], Move, NewBoard, NewPlayer), % Makes the move
        computeReply(NewBoard, NewPlayer, Reply),!. % replies the new board and player
        
parse_msg(move(_, _, _, _), no) :- !.

% *********************************** Computer player ***********************************
parse_msg(move(Board, [Color,'C1', Score]), Reply) :-
        getRandomMove(Board, [Color,'C1', Score], Move),
        makeMove(Board, [Color,'C1', Score], Move, NewBoard, NewPlayer), % Makes the move
        computeReply(NewBoard, NewPlayer, Reply), !. % replies the new board and player

parse_msg(move(Board, [Color,'C2', Score]), Reply) :-
        getSmartMove(Board, [Color,'C2', Score], Move),
        makeMove(Board, [Color,'C2', Score], Move, NewBoard, NewPlayer), % Makes the move
        computeReply(NewBoard, NewPlayer, Reply), !. % replies the new board and player

parse_msg(move(_, _), no) :- !.

% *********************************** Quit ***********************************
parse_msg(quit, bye) :- !.
parse_msg(end_of_file, bye) :- !.

parse_msg(_, invalid) :- !.

computeReply(NewBoard, NewPlayer, ReplyOut) :-
        playerWon(NewPlayer),
        ReplyOut = yes(NewBoard, NewPlayer, won), !.

computeReply(NewBoard, NewPlayer, ReplyOut) :-
        ReplyOut = yes(NewBoard, NewPlayer, no), !.



% *******************************************************************************************
% *********************************** Auxiliar Predicates ***********************************
% *******************************************************************************************

validPositions(InitialPosition, FinalPosition) :-
        validPosition(InitialPosition),
        validPosition(FinalPosition).

validPosition([X|Y]) :-
        X >= 0, X =< 7,
        Y >= 0, Y =< 7,
        !.


checkValidMove(Board, Player, InitialPosition, FinalPosition, MoveOut) :-
        validPositions(InitialPosition, FinalPosition), % Check if the positions are in range
        getUserMove(Board, InitialPosition, FinalPosition, MoveOut), % Transform InitialPosition,FinalPosition into a Move structure
        validMove(Board, Player, MoveOut). % Check if the move is valid according to the current game

% --------------- Gets a User Move, returning the piece code, initial position, N and Local Direction --------------
getUserMove(Board, InitialPosition, FinalPosition, [PieceOut, Nout, DirectionOut]) :-
        getPieceCoord(Board, InitialPosition, PieceOut), isPiece(PieceOut), % Get the piece in the initial position (valid Piece!)
        calculateDirection(PieceOut, InitialPosition, FinalPosition, DirectionOut), % Calculate local direction based on positions
        calculateN(InitialPosition, FinalPosition, Nout), % Calculate N based on positions
        !.


% ***************** DEBUG *****************************

printBoard(Board) :- cleanScreen, printXAxis, printSeparator, printBoardAux(Board, [0|7]).
printBoardAux([], [_|-1]) :- printXAxis, nl.
printBoardAux([A|B], [X|Y]) :-
        write(Y), Ynew is Y-1,
        printLine(A, [X|Y]), write(Y), nl, printSeparator,
        printBoardAux(B, [X|Ynew]).

cleanScreen :- nl, nl, nl, nl, nl, nl, nl, nl.
printXAxis :- write('    0   1   2   3   4   5   6   7'), nl.

printLine([], [_|_]) :- write(' | ').
printLine([A|B], [X|Y]) :- write(' | '), translate(A, [X|Y], PrintName), write(PrintName), Xnew is X+1, printLine(B, [Xnew|Y]).

translate(' ', [X|Y], 'x') :- trench([X|Y]), !. % free trench space
translate(' ', _, ' ') :- !. % free normal space
translate(I, _, 'E') :- blackSoldier(I), !.
translate(I, _, 'e') :- whiteSoldier(I), !.
translate(I, _, 'D') :- blackSargent(I), !.
translate(I, _, 'd') :- whiteSargent(I), !.
translate(I, _, 'C') :- blackCaptain(I), !.
translate(I, _, 'c') :- whiteCaptain(I), !.
translate(I, _, 'B') :- blackColonel(I), !.
translate(I, _, 'b') :- whiteColonel(I), !.
translate(I, _, 'A') :- blackGeneral(I), !.
translate(I, _, 'a') :- whiteGeneral(I), !.

printSeparator :- write('   _______________________________'), nl.