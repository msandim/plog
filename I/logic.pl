:- use_module(library(lists)).
:- use_module(library(random)).

:- consult(player).

% ************************* GAME OVER CONDITION *******************************

% Player1 is the winner and Player2 lost        
gameOver(Player1, Player2, Player1, Player2) :-
        playerScore(Player1, ScorePlayer1),
        playerScore(Player2, ScorePlayer2),
        ScorePlayer1 >= 50,
        ScorePlayer2 < 50.

% Player2 is the winner and Player1 lost
gameOver(Player1, Player2, Player2, Player1) :-
        playerScore(Player1, ScorePlayer1),
        playerScore(Player2, ScorePlayer2),
        ScorePlayer1 < 50,
        ScorePlayer2 >= 50.

playerWon(Player) :-
        playerScore(Player, Score),
        Score >= 50.


% *********************************************************************************
% ***************************** AI DECISION MAKING ********************************
% *********************************************************************************

getAllMoves(Board, Player, MoveListOut) :-
        findall(Move, validMove(Board, Player, Move), MoveListOut).
        %write('Moves found: '), write(MoveListOut), nl.

% ------------------------------- RANDOM DECISION ---------------------------------

getRandomMove(Board, Player, MoveOut) :-
        getAllMoves(Board, Player, MoveList), % Get the MoveList
        same_length(MoveList, MoveList, ListLength), % Get length of the MoveList
        random(0, ListLength, ListIndex), % Generate index between [0, MoveListLength[
        nth0(ListIndex, MoveList, MoveOut). % Access that element!

% ------------------------------ SMART MOVE --------------------------------------

% Predicate that gets the smartest move for Player at the moment
getSmartMove(Board, Player, MoveOut) :-
        getAllMoves(Board, Player, MoveList), % Get the move list
        calcMoveScoreList(Board, Player, MoveList, MoveScoreList), % Calculate the value for each move
        %write('Move scores: '), write(MoveScoreList), nl,
        max_member(MaxMoveScore, MoveScoreList), % Get the maximum score possible
        nth0(MoveIndex, MoveScoreList, MaxMoveScore), % Get the index of the Move with the maximum score
        nth0(MoveIndex, MoveList, MoveOut). % Get the move with that index in the MoveList

% Calculate the Score List for all moves
calcMoveScoreList(_, _, [], []).
calcMoveScoreList(Board, Player, [Move|MoveL], [MoveScore|MoveScoreL]) :-
        calcMoveScore(Board, Player, Move, MoveScore), % Calculate score for one singular Move
        calcMoveScoreList(Board, Player, MoveL, MoveScoreL). % Iterate the list calculation

% Calculate the score for a Move
calcMoveScore(Board, Player, Move, MoveScoreOut) :-
        makeMove(Board, Player, Move, NewBoard, NewPlayer), % Execute the Move in the board
        calcScoreGain(Player, NewPlayer, PlayerScoreGain), % Calculate the Player's Score gain
        getPlayerColor(Player, PlayerColor), calcTrenchGain(Board, NewBoard, PlayerColor, TrenchScoreGain), % Calculate the Trench Score gain
        TrenchScore is TrenchScoreGain//4, % Give more power to the Player's Score than the Trench Score
        MoveScoreOut is PlayerScoreGain + TrenchScore.
        

% *********************************************************************************
% ************************** VALID MOVE CHECKS ************************************
% *********************************************************************************

validN(1).
validN(2).
validN(3).
validN(4).
validN(5).

% Valid certification predicate:
validMove(Board, Player, [Piece, N, Direction]) :-
        validN(N),
        sameColor(Player, Piece), % A player can only move his pieces
        isValidDirection(Direction, Piece), % Each piece has restricted directions
        canMoveSquares(Piece, N), % Each piece has restricted number of moves
        isPathValid(Board, [Piece, N, Direction]). % The path must be valid
        
% Checks if a move goes through a free path on the board (last position can have either an enemy or an empty position
isPathValid(Board, [Piece, N, Direction]) :-
        getPieceCoord(Board, Position, Piece), % Get the coordinate in the board for the piece
        isPathValidAux(Board, Piece, Position, N, Direction).

% ********************* MOVE CONDITIONS ************************************
% ---------------- STARTING AT TRENCH ------------------------------
% If is in the trench, and the direction L/R, must not encounter any piece
isPathValidAux(Board, Piece, Position, N, Direction) :-
        trench(Position),
        sideDirection(Direction),
        iterateValidPath(Board, Piece, Position, N, Direction, 'NO PIECES').

% If is in the trench, and moving Forward (to enemy territory), it can eat multiple enemy pieces
isPathValidAux(Board, Piece, Position, N, Direction) :-
        trench(Position),
        forwardDirection(Direction),
        iterateValidPath(Board, Piece, Position, N, Direction, 'ENEMY PIECES').

% If is in the trench, and moving Backward (to friendly territory), it cannot eat enemy pieces
isPathValidAux(Board, Piece, Position, N, Direction) :-
        trench(Position),
        backwardDirection(Direction),
        iterateValidPath(Board, Piece, Position, N, Direction, 'NO PIECES').

% ---------------- STARTING AT TERRITORY ------------------------------
% If it's in friendly territory (maybe reaching trench)
isPathValidAux(Board, Piece, Position, N, Direction) :-
        friendlyTerritory(Piece, Position),
        iterateValidPath(Board, Piece, Position, N, Direction, 'LAST PIECE ENEMY NOT IN TRENCH').

% If it's in enemy territory (maybe reaching trench)
isPathValidAux(Board, Piece, Position, N, Direction) :-
        enemyTerritory(Piece, Position),
        iterateValidPath(Board, Piece, Position, N, Direction, 'LAST PIECE ENEMY').

% ****************** ITERATE THROUGH THE PATH AND CHECK CONDITIONS ******************
% ---------------------- NO PIECES ALLOWED DURING PATH ------------------------------
iterateValidPath(_, _, _, 0, _, 'NO PIECES').
iterateValidPath(Board, Piece, Position, N, Direction, 'NO PIECES') :-
        calcNextPosition(Piece, Position, 1, Direction, NewPosition), % Iterate to the next position
        isEmptySquare(Board, NewPosition), % The next position must have no pieces
        N1 is N-1,
        iterateValidPath(Board, Piece, NewPosition, N1, Direction, 'NO PIECES').

% ---------------------- ONLY ENEMY PIECES ALLOWED DURING PATH  --------------------
iterateValidPath(_, _, _, 0, _, 'ENEMY PIECES').
iterateValidPath(Board, Piece, Position, N, Direction, 'ENEMY PIECES') :-
        calcNextPosition(Piece, Position, 1, Direction, NewPosition), % Iterate to the next position
        canMoveTo(Piece, Board, NewPosition), % The next position must either enemy or free
        N1 is N-1,
        iterateValidPath(Board, Piece, NewPosition, N1, Direction, 'ENEMY PIECES').

% ---------------------- ONLY THE LAST CAN BE PIECE (ENEMY)  -----------------------
iterateValidPath(Board, Piece, Position, 1, Direction, 'LAST PIECE ENEMY') :-
        calcNextPosition(Piece, Position, 1, Direction, NewPosition), % Iterate to the next position
        canMoveTo(Piece, Board, NewPosition). % The next position must either enemy or free       
iterateValidPath(Board, Piece, Position, N, Direction, 'LAST PIECE ENEMY') :-
        calcNextPosition(Piece, Position, 1, Direction, NewPosition), % Iterate to the next position
        isEmptySquare(Board, NewPosition), % The next position must have no pieces
        N1 is N-1,
        iterateValidPath(Board, Piece, NewPosition, N1, Direction, 'LAST PIECE ENEMY').

% ---------------------- ONLY THE LAST CAN BE PIECE (ENEMY) IF NOT TRENCH  ---------
iterateValidPath(Board, Piece, Position, 1, Direction, 'LAST PIECE ENEMY NOT IN TRENCH') :-
        calcNextPosition(Piece, Position, 1, Direction, NewPosition), % Iterate to the next position
        trench(NewPosition), !, % Must not be at trench!!    
        isEmptySquare(Board, NewPosition). % The next position must be free

iterateValidPath(Board, Piece, Position, 1, Direction, 'LAST PIECE ENEMY NOT IN TRENCH') :-
        calcNextPosition(Piece, Position, 1, Direction, NewPosition), % Iterate to the next position
        canMoveTo(Piece, Board, NewPosition). % The next position must either enemy or free
     
iterateValidPath(Board, Piece, Position, N, Direction, 'LAST PIECE ENEMY NOT IN TRENCH') :-
        calcNextPosition(Piece, Position, 1, Direction, NewPosition), % Iterate to the next position
        isEmptySquare(Board, NewPosition), % The next position must have no pieces
        N1 is N-1,
        iterateValidPath(Board, Piece, NewPosition, N1, Direction, 'LAST PIECE ENEMY NOT IN TRENCH').


% **************************************************************************************************
% ******************************* Move making predicates *******************************************
% **************************************************************************************************

% Makes a move that is already confirmed as valid!
% it will update the Board and Player score (returning in new lists)
% If moving forward from the trench -> analyze each movement as more than 1 piece may be eaten
makeMove(Board, Player, [Piece, N, Direction], BoardOut, PlayerOut) :-
        isAtTrench(Board, Piece), 
        forwardDirection(Direction), !,
        makeStepMove(Board, Player, [Piece, N, Direction], BoardOut, PlayerOut).

% If not, treate as a normal movement
makeMove(Board, Player, Move, BoardOut, PlayerOut) :-
        makeFullMove(Board, Player, Move, BoardOut, PlayerOut).
        
% Unificate Board and Player with the new ones!
makeStepMove(Board, Player, [_, 0, _], Board, Player).
makeStepMove(Board, Player, [Piece, N, Direction], BoardOut, PlayerOut) :-
        N > 0,
        N1 is N-1,
        makeFullMove(Board, Player, [Piece, 1, Direction], BoardSemiOut, PlayerSemiOut), % Iterate full move with N = 1
        makeStepMove(BoardSemiOut, PlayerSemiOut, [Piece, N1, Direction], BoardOut, PlayerOut).

makeFullMove(Board, Player, [Piece, N, Direction], BoardOut, PlayerOut) :-
        getPieceCoord(Board, PiecePosition, Piece), % get Position of the Piece to move
        calcNextPosition(Piece, PiecePosition, N, Direction, FinalPosition), % Calculate the position to land the piece
        getPieceCoord(Board, FinalPosition, FinalPiece), % Get Position of the Piece on the place to move to
        erasePiece(Piece, Board, BoardSemiOut), % Piece is erased from the board: it is replaced by a ' '
        setPiece(Piece, FinalPosition, BoardSemiOut, BoardOut), % Piece is placed on the new location
        updatePlayerScore(Player, FinalPiece, PlayerOut). % Update the player's score if a catched an enemy piece


% **************************************************************************************************
% ******************************* Other: Direction Check *******************************************
% **************************************************************************************************

forwardDirection('F').
forwardDirection('FR').
forwardDirection('FL').
backwardDirection('B').
backwardDirection('BR').
backwardDirection('BL').
sideDirection('L').
sideDirection('R').

directionVectorsList(
	[
		['F', 'FR', 'FL', 'R', 'L', 'B', 'BR', 'BL'],
		[1, -1], [0, -1], [1, 0],
		[-1, -1], [1, 1],
		[-1, 1], [-1, 0], [0, 1]
	]).
getDirectionVector(DirectionIn, [DirectionIn|_], [VectorOut|_], VectorOut) :- !.
getDirectionVector(DirectionIn, [_|RD], [_|RV], VectorOut) :- getDirectionVector(DirectionIn, RD, RV, VectorOut).
getDirectionVector(DirectionIn, VectorOut) :-
		directionVectorsList([DirectionsList|VectorsList]),
		getDirectionVector(DirectionIn, DirectionsList, VectorsList, VectorOut).

calcNextPosition(Piece, [Xi|Yi], N, DirectionIn, [Xf|Yf]) :-
		white(Piece), !,
		getDirectionVector(DirectionIn, [Vx, Vy]),
		Xf is Xi + (N * Vx), 
		Yf is Yi + (N * Vy).
calcNextPosition(_, [Xi|Yi], N, DirectionIn, [Xf|Yf]) :-
		getDirectionVector(DirectionIn, [Vx|Vy]),
		Xf is Xi - (N * Vx),
		Yf is Yi - (N * Vy).

commonPieceDirectionList(['FR', 'FL', 'BR', 'BL']).
pieceDirectionList(
	[
		[sargent, captain, colonel, general],
		['F'],
		['F', 'B'],
		['F', 'R', 'L'],
		['F', 'B', 'R', 'L']
	]).

isValidDirection(DirectionIn, Piece, [Rank|_], [RankDirections|_]) :-
		Function =.. [Rank, Piece],
		Function, !,
		member(DirectionIn, RankDirections).
isValidDirection(DirectionIn, Piece, [_|RR], [_|RD]) :-
		isValidDirection(DirectionIn, Piece, RR, RD).

% Every piece can move in the directions specified in common list:
isValidDirection(DirectionIn, _) :-
		commonPieceDirectionList(DirectionsList),
		member(DirectionIn, DirectionsList).

% For other directions, we must check the ranks of the piece:
isValidDirection(DirectionIn, Piece) :- 
		pieceDirectionList([RanksList|DirectionsList]),
		isValidDirection(DirectionIn, Piece, RanksList, DirectionsList).