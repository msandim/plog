:- use_module(library(lists)).

:- include(piece).

% ----- create Board -----
initialBoard([[151,142,133,124,' ',' ',' ',' '],
              [141,132,123,116,' ',' ',' ',' '],
              [131,122,115,113,' ',' ',' ',' '],
              [121,114,112,111,' ',' ',' ',' '],
              [' ',' ',' ',' ',211,213,216,224],
              [' ',' ',' ',' ',212,215,223,233],
              [' ',' ',' ',' ',214,222,232,242],
              [' ',' ',' ',' ',221,231,241,251]
              ]).
        
% ----------------- Misc. functions regarding black and white ------------------------        
trench([X|Y]) :- X == Y.
whiteTerritory([X|Y]) :- X < Y.
blackTerritory([X|Y]) :- X > Y.

enemyTerritory(Piece, Position) :- black(Piece), !, whiteTerritory(Position).
enemyTerritory(_, Position) :- blackTerritory(Position).

friendlyTerritory(Piece, Position) :- black(Piece), !, blackTerritory(Position).
friendlyTerritory(_, Position) :- whiteTerritory(Position).

isAtEnemyTerritory(Board, Piece) :- black(Piece), !, getPieceCoord(Board, Position, Piece), whiteTerritory(Position).
isAtEnemyTerritory(Board, Piece) :- getPieceCoord(Board, Position, Piece), blackTerritory(Position).

isAtTrench(Board, Piece) :- getPieceCoord(Board, Position, Piece), trench(Position).

isPiece(Board, Position) :- isWhitePiece(Board, Position), !.
isPiece(Board, Position) :- isBlackPiece(Board, Position), !.

isEnemyPieceAt(Board, Piece, Position) :- black(Piece), !, isWhitePiece(Board, Position).
isEnemyPieceAt(Board, _, Position) :- isBlackPiece(Board, Position).

isWhitePiece(Board, Position) :- getPieceCoord(Board, Position, PieceOut), white(PieceOut). 
isBlackPiece(Board, Position) :- getPieceCoord(Board, Position, PieceOut), black(PieceOut).

isEmptySquare(Board, Position) :- getPieceCoord(Board, Position, PieceOut), PieceOut == ' ', !.

% ------------------- Count the difference in the number of pieces in Trench of 2 Boards -----------
calcTrenchGain(Board1, Board2, Color, NumberOut) :-
        countTrenchPieceScore(Board1, Color, Number1),
        countTrenchPieceScore(Board2, Color, Number2),
        NumberOut is Number2 - Number1.

% ------------------- Count the score of pieces in the Trench -------------------------------------
countTrenchPieceScore(Board, Color, NumberOut) :-
        countTrenchPieceScoreAux(Board, Color, [0|0], 0, NumberOut).

countTrenchPieceScoreAux(_, _, [8|8], Acum, Acum).

countTrenchPieceScoreAux(Board, 'B', [X|Y], Acum, NumberOut) :-
        X =< 7, Y =< 7,
        getPieceCoord(Board, [X|Y], Piece),
        isPiece(Piece), black(Piece),
        pieceScore(Piece, PieceScore),
        Xnew is X+1, Ynew is Y+1, NewAcum is Acum + PieceScore, !,
        countTrenchPieceScoreAux(Board, 'B', [Xnew|Ynew], NewAcum, NumberOut).

countTrenchPieceScoreAux(Board, 'W', [X|Y], Acum, NumberOut) :-
        X =< 7, Y =< 7,
        getPieceCoord(Board, [X|Y], Piece),
        isPiece(Piece), white(Piece),
        pieceScore(Piece, PieceScore),
        Xnew is X+1, Ynew is Y+1, NewAcum is Acum + PieceScore, !,
        countTrenchPieceScoreAux(Board, 'W', [Xnew|Ynew], NewAcum, NumberOut).

countTrenchPieceScoreAux(Board, Color, [X|Y], Acum, NumberOut) :-
        X =< 7, Y =< 7,
        Xnew is X+1, Ynew is Y+1, !,
        countTrenchPieceScoreAux(Board, Color, [Xnew|Ynew], Acum, NumberOut).        

% ------------------- Get Piece or Get Coord: YOU choose :) ----------
 getPieceCoord(Board, [X|Y], Piece) :- 
        nth0(Ytrans, Board, LineOut),
        Y is 7 - Ytrans, 
        nth0(X, LineOut, Piece). 

% ------------------ Erase Piece ---------------------------------
erasePiece(Piece, Board, BoardOut) :-
        erasePieceAux(Piece, [0|7], Board, BoardOut).
        
erasePieceAux(Piece, [X|Y], [H|L], [Hout|Lout]) :-
        erasePieceLine(Piece, [X|Y], H, Hout),
        Ynew is Y-1,
        erasePieceAux(Piece, [X|Ynew], L, Lout).
erasePieceAux(_, [_|_], [], []).
             
erasePieceLine(Piece, [_|_], [Piece|L], [' '|L]) :- !.                 % Substitute piece with space char
erasePieceLine(Piece, [X|Y], [H|L], [H|Lout]) :-                       % Continue iterating
        Xnew is X+1,
        erasePieceLine(Piece, [Xnew|Y], L, Lout),
        !.
erasePieceLine(_, [_|_], [], []) :- !. % Terminal case

% --------------------- Set Piece ---------------------------------
setPiece(Piece, [X|7], [H|L], [Hout|L]) :-
        setPieceLine(Piece, [X|0], H, Hout),
        !.
setPiece(Piece, [X|Y], [H|L], [H|Lout]) :-
        Y =< 7,
        Ynew is Y+1,
        setPiece(Piece, [X|Ynew], L, Lout),
        !.

setPieceLine(Piece, [0|0], [_|L], [Piece|L]) :- !.
setPieceLine(Piece, [X|0], [H|L], [H|Lout]) :- 
        Xnew is X-1,
        setPieceLine(Piece, [Xnew|0], L, Lout),
        !.

% --------- Predicate that indicates if a piece can be set on that position ------------------------
canMoveTo(_, Board, Position) :- isEmptySquare(Board, Position), !.
canMoveTo(Piece, Board, Position) :- 
    white(Piece), !, 
    isBlackPiece(Board, Position).
canMoveTo(_, Board, Position) :- 
    isWhitePiece(Board, Position).