isPiece(I) :- integer(I), white(I), !.
isPiece(I) :- integer(I), black(I), !.

black(I) :- blackSoldier(I).
black(I) :- blackSargent(I).
black(I) :- blackCaptain(I).
black(I) :- blackColonel(I).
black(I) :- blackGeneral(I).

white(I) :- whiteSoldier(I).
white(I) :- whiteSargent(I).
white(I) :- whiteCaptain(I).
white(I) :- whiteColonel(I).
white(I) :- whiteGeneral(I).

% ------- Soldiers --------
soldier(I) :- blackSoldier(I), !.
soldier(I) :- whiteSoldier(I).
blackSoldier(211).
blackSoldier(212).
blackSoldier(213).
blackSoldier(214).
blackSoldier(215).
blackSoldier(216).

whiteSoldier(111).
whiteSoldier(112).
whiteSoldier(113).
whiteSoldier(114).
whiteSoldier(115).
whiteSoldier(116).

% ------- Sargents --------
sargent(I) :- blackSargent(I), !.
sargent(I) :- whiteSargent(I).
blackSargent(221).
blackSargent(222).
blackSargent(223).
blackSargent(224).

whiteSargent(121).
whiteSargent(122).
whiteSargent(123).
whiteSargent(124).

% ------- Captains --------
captain(I) :- blackCaptain(I), !.
captain(I) :- whiteCaptain(I).
blackCaptain(231).
blackCaptain(232).
blackCaptain(233).

whiteCaptain(131).
whiteCaptain(132).
whiteCaptain(133).

% ------- Colonels --------
colonel(I) :- blackColonel(I), !.
colonel(I) :- whiteColonel(I).
blackColonel(241).
blackColonel(242).

whiteColonel(141).
whiteColonel(142).

% ------- Generals --------
general(I) :- blackGeneral(I), !.
general(I) :- whiteGeneral(I).
blackGeneral(251).
whiteGeneral(151).

% Validates a number of squares a piece can advance
canMoveSquares(Piece, 1) :- soldier(Piece).
canMoveSquares(Piece, 2) :- sargent(Piece).
canMoveSquares(Piece, 3) :- captain(Piece).
canMoveSquares(Piece, 4) :- colonel(Piece).
canMoveSquares(Piece, 5) :- general(Piece).
canMoveSquares(Piece, Squares) :-
        Squares > 0,
        N1 is Squares + 1, 
        N1 =< 5, !,
        canMoveSquares(Piece, N1).

% Gets the score gained by eliminating this piece
pieceScore(Piece, 2) :- soldier(Piece).
pieceScore(Piece, 4) :- sargent(Piece).
pieceScore(Piece, 6) :- captain(Piece).
pieceScore(Piece, 8) :- colonel(Piece).
pieceScore(Piece, 10) :- general(Piece).