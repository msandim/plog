:- include(input).

% ************************************************************************
% *********************** INITIATE GAME BY CLI ***************************
% ************************************************************************

trench :-
        readPlayerConfiguration(BlackPlayer, WhitePlayer),
        runGame(BlackPlayer, WhitePlayer).

readPlayerConfiguration(['B', Type1, 0], ['W', Type2, 0]) :-
        readPlayerInfoCli('B', Type1), % Get configuration for black player
        readPlayerInfoCli('W', Type2). % Get configuration for gui player               


% ****************************************************************************
% ******************************* RUN GAME CYCLE *****************************
% ****************************************************************************

% Runs the game with 2 Players defined
runGame(BlackPlayer, WhitePlayer) :-
        initialBoard(Board),
        !,
        play(Board, BlackPlayer, WhitePlayer). % Black player starts playing

play(_, Player1, Player2) :-
        gameOver(Player1, Player2, Winner, Loser),
        !,
        displayFinalResult(Winner, Loser).

play(Board, Player1, Player2) :-
        printBoard(Board),
        printPlayersState(Player1, Player2),
        chooseMove(Board, Player1, Move), write('Chosen move was: '), write(Move), nl,
        makeMove(Board, Player1, Move, NewBoard, NewPlayer1),
        !,
        play(NewBoard, Player2, NewPlayer1). % Change player turns

% Chooses a move from user input
chooseMove(Board, [Color,'H', Score], MoveOut) :- 
        getUserMove(Board, MoveOut),
        validMove(Board, [Color,'H', Score], MoveOut).

chooseMove(Board, [Color,'H', Score], MoveOut) :- 
        write('Invalid Move, choose another one!'), nl,
        chooseMove(Board, [Color,'H', Score], MoveOut).

% Chooses a move from the computer (randomly)
chooseMove(Board, [Color,'C1', Score], MoveOut) :- 
        getRandomMove(Board, [Color,'C1', Score], MoveOut).

% Chooses a move from the computer (smart move)
chooseMove(Board, [Color,'C2', Score], MoveOut) :- 
        getSmartMove(Board, [Color,'C2', Score], MoveOut).

%********************************************************************************************************************
%********************************************************************************************************************
% ******************************************* INPUT COMPUTING *******************************************************
%********************************************************************************************************************
%********************************************************************************************************************

% --------------- Gets a User Move, returning the piece code, initial position, N and Local Direction --------------
getUserMove(Board, [PieceOut, Nout, DirectionOut]) :-
        readPositionsCli(InitialPosition, FinalPosition), % Read initial/final position from command line
        getPieceCoord(Board, InitialPosition, PieceOut), isPiece(PieceOut), % Get the piece in the initial position (valid Piece!)
        calculateDirection(PieceOut, InitialPosition, FinalPosition, DirectionOut), % Calculate local direction based on positions
        calculateN(InitialPosition, FinalPosition, Nout), % Calculate N based on positions
        !.
getUserMove(Board, MoveOut) :-
        write('Invalid input.'), nl,
        getUserMove(Board, MoveOut).

% ********************************************************************
% ************************ Print Predicates **************************
% ********************************************************************

% ------------------------ Print Board -------------------------------
printTest :- initialBoard(X), printBoard(X).

printBoard(Board) :- cleanScreen, printXAxis, printSeparator, printBoardAux(Board, [0|7]).
printBoardAux([], [_|-1]) :- printXAxis, nl.
printBoardAux([A|B], [X|Y]) :-
        write(Y), Ynew is Y-1,
        printLine(A, [X|Y]), write(Y), nl, printSeparator,
        printBoardAux(B, [X|Ynew]).

cleanScreen :- nl, nl, nl, nl, nl, nl, nl, nl.
printXAxis :- write('    0   1   2   3   4   5   6   7'), nl.

printLine([], [_|_]) :- write(' | ').
printLine([A|B], [X|Y]) :- write(' | '), translate(A, [X|Y], PrintName), write(PrintName), Xnew is X+1, printLine(B, [Xnew|Y]).

translate(' ', [X|Y], 'x') :- trench([X|Y]), !. % free trench space
translate(' ', _, ' ') :- !. % free normal space
translate(I, _, 'E') :- blackSoldier(I), !.
translate(I, _, 'e') :- whiteSoldier(I), !.
translate(I, _, 'D') :- blackSargent(I), !.
translate(I, _, 'd') :- whiteSargent(I), !.
translate(I, _, 'C') :- blackCaptain(I), !.
translate(I, _, 'c') :- whiteCaptain(I), !.
translate(I, _, 'B') :- blackColonel(I), !.
translate(I, _, 'b') :- whiteColonel(I), !.
translate(I, _, 'A') :- blackGeneral(I), !.
translate(I, _, 'a') :- whiteGeneral(I), !.

printSeparator :- write('   _______________________________'), nl.

% ----------------------- Print Player Info ---------------------------
printPlayer([Color, Type, Score]) :- printPlayerColor(Color), write(' | '), printPlayerType(Type), write(' | '), print(Score), nl.
printPlayerColor('B') :- write('BLACK').
printPlayerColor('W') :- write('white').
printPlayerType('H') :- write('Human').
printPlayerType('C1') :- write('Computer (Random Moves)').
printPlayerType('C2') :- write('Computer (Smart Moves)').

printPlayersState(Player1, Player2) :-
        write('Current: '), printPlayer(Player1),
        write('Other: '), printPlayer(Player2), nl.

% ----------------------- Print Game Over ---------------------------

displayFinalResult(Winner, Loser) :-
        nl, write('*** Game ended - Results ***'), nl,
        write('Winner: '), printPlayer(Winner),
        write('Loser: '), printPlayer(Loser).

% ************************************************************************************
% *************************** Input reading predicates *******************************
% ************************************************************************************

% --------------- Reads a Player Type from the User (H, C1, C2) --------------
readPlayerInfoCli(Color, TypeOut) :-
        nl, write('*** '), printPlayerColor(Color), write(' Player ***'), nl,
        write('Insert player type:'), nl,
        write('(1) - Human'), nl,
        write('(2) - Computer with Random Moves'), nl,
        write('(3) - Computer with AI'), nl,
        readTypeCli(TypeOut).

validPlayerType(1, 'H').
validPlayerType(2, 'C1').
validPlayerType(3, 'C2').

readTypeCli(TypeOut) :-
        read_line([Input|_]),
        Type is Input-48,
        validPlayerType(Type, TypeOut),
        !.

readTypeCli(TypeOut) :-
        write('Invalid Option'), nl,
        readTypeCli(TypeOut).

% Reads a position
readPositionCli([X|Y]) :-
        read_line(Line), nth0(0, Line, Xsemi), X is Xsemi-48, nth0(2, Line, Ysemi), Y is Ysemi-48,
        X >= 0, X =< 7,
        Y >= 0, Y =< 7,
        !.
readPositionCli(X) :-
        write('Invalid Position'), nl,
        readPositionCli(X).

% Reads two positions
readPositionsCli(InitialPositionOut, FinalPositionOut) :-
        write('Insert initial position (X,Y): '), readPositionCli(InitialPositionOut),
        write('Insert final position (X,Y): '), readPositionCli(FinalPositionOut).