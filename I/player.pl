:- include(board).

initialPlayerBlack(['B', 'H', 0]). % Piece Color, Human/Computer, Score
initialPlayerWhite(['W', 'H', 0]).

playerScore([_, _, Score], Score).
isPlayerHuman([_, 'H', _]).

getPlayerColor([Color, _, _], Color).

sameColor(['B',_,_], Piece) :- black(Piece).
sameColor(['W',_,_], Piece) :- white(Piece).

calcScoreGain([_, _, OldScore], [_, _, NewScore], ScoreDiffOut) :-
        ScoreDiffOut is NewScore - OldScore.

updatePlayerScore([Color, Type, Score], PieceEliminated, [Color, Type, NewScore]) :- % Update user score if the eliminated piece is valid
        isPiece(PieceEliminated), !,
        pieceScore(PieceEliminated, ScoreInc),
        NewScore is Score + ScoreInc.
updatePlayerScore(Player, _, Player). % Make the same, if there is no piece
        