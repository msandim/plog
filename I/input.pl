:- include(logic).

% ------------ Calculates Direction based on the positions and the piece ------------------------------
calculateDirection(Piece, InitialPosition, FinalPosition, DirectionOut) :-
        calculateGlobalDirection(InitialPosition, FinalPosition, GlobalDirection), % Get Global Direction
        globalToLocalDirection(Piece, GlobalDirection, DirectionOut). % Transform Global Direction into Piece's Local Direction

calculateGlobalDirection([Xini|Yini], [Xfinal|Yfinal], 'N') :- Xfinal == Xini, Yfinal > Yini, !.
calculateGlobalDirection([Xini|Yini], [Xfinal|Yfinal], 'NE') :- Xfinal > Xini, Yfinal > Yini, !.
calculateGlobalDirection([Xini|Yini], [Xfinal|Yfinal], 'E') :- Xfinal > Xini, Yfinal == Yini, !.
calculateGlobalDirection([Xini|Yini], [Xfinal|Yfinal], 'SE') :- Xfinal > Xini, Yfinal < Yini, !.
calculateGlobalDirection([Xini|Yini], [Xfinal|Yfinal], 'S') :- Xfinal == Xini, Yfinal < Yini, !.
calculateGlobalDirection([Xini|Yini], [Xfinal|Yfinal], 'SW') :- Xfinal < Xini, Yfinal < Yini, !.
calculateGlobalDirection([Xini|Yini], [Xfinal|Yfinal], 'W') :- Xfinal < Xini, Yfinal == Yini, !.
calculateGlobalDirection([Xini|Yini], [Xfinal|Yfinal], 'NW') :- Xfinal < Xini, Yfinal > Yini, !.  

% ------------ Calculates N based on the positions and the piece ------------------------------
calculateN([Xini|Yini], [Xfinal|Yfinal], Nout) :-
        DeltaXsemi is Xfinal - Xini,
        DeltaX is abs(DeltaXsemi),
        DeltaYsemi is Yfinal - Yini,
        DeltaY is abs(DeltaYsemi),
        validateDelta(DeltaX, DeltaY),
        Nsemi is max(DeltaX, DeltaY),
        Nout is abs(Nsemi).

validateDelta(0, _).
validateDelta(_, 0).
validateDelta(X, X).

% -------------------------- Auxiliar: Global to Local Direction transformer ------------------------
globalToLocalDirectionList(
        [
                ['E', 'NE', 'N', 'NW', 'W', 'SW', 'S', 'SE'],
                ['FL', 'L', 'BL', 'B', 'BR', 'R', 'FR', 'F'],
                ['BR', 'R', 'FR', 'F', 'FL', 'L', 'BL', 'B']
        ]).

globalToLocalDirection(_, GlobalIn, [GlobalIn|_], [LocalOut|_], LocalOut) :- !.
globalToLocalDirection(Piece, GlobalIn, [_|RG], [_|RL], LocalOut) :-
                globalToLocalDirection(Piece, GlobalIn, RG, RL, LocalOut).
                
globalToLocalDirection(Piece, GlobalIn, LocalOut) :-
        white(Piece), !,
        globalToLocalDirectionList([GlobalList|[LocalList|_]]),     % Get globalToLocalDirectionList[1]
        globalToLocalDirection(Piece, GlobalIn, GlobalList, LocalList, LocalOut).
globalToLocalDirection(Piece, GlobalIn, LocalOut) :-
        globalToLocalDirectionList([GlobalList|[_|[LocalList|_]]]), % Get globalToLocalDirectionList[2]
        globalToLocalDirection(Piece, GlobalIn, GlobalList, LocalList, LocalOut).